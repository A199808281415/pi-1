import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter as Router, Route } from 'react-router-dom'

import './style.css'
import Perfil from './views/perfil'
import Page from './views/page'
import Home from './views/home'
import axios from 'axios'

const get_from_backend = async () => {
  let result = await axios.get("localhost:8000/professor")

  console.log(result.data)

  return result.data
}

const App = () => {
  return (
    <Router>
      <div>
        <Route component={Perfil} data={get_from_backend()} exact path="/perfil" />
        <Route component={Page} exact path="/page" />
        <Route component={Home} exact path="/" />
      </div>
    </Router>
  )
}

ReactDOM.render(<App />, document.getElementById('app'))
