import React from 'react'

import DangerousHTML from 'dangerous-html/react'
import { Helmet } from 'react-helmet'

import NavigationLinks from '../components/navigation-links'
import './perfil.css'

const Perfil = (props) => {
  return (
    <div className="perfil-container">
      <Helmet>
        <title>perfil - Medica template</title>
        <meta property="og:title" content="perfil - Medica template" />
      </Helmet>
      <div data-modal="practices" className="perfil-modal">
        <div className="perfil-practices">
          <div className="perfil-heading">
            <span className="perfil-header">Our practices</span>
            <svg
              viewBox="0 0 1024 1024"
              data-close="practices"
              className="perfil-close"
            >
              <path d="M225.835 286.165l225.835 225.835-225.835 225.835c-16.683 16.683-16.683 43.691 0 60.331s43.691 16.683 60.331 0l225.835-225.835 225.835 225.835c16.683 16.683 43.691 16.683 60.331 0s16.683-43.691 0-60.331l-225.835-225.835 225.835-225.835c16.683-16.683 16.683-43.691 0-60.331s-43.691-16.683-60.331 0l-225.835 225.835-225.835-225.835c-16.683-16.683-43.691-16.683-60.331 0s-16.683 43.691 0 60.331z"></path>
            </svg>
          </div>
          <div className="perfil-grid">
            <div className="perfil-section">
              <div className="perfil-heading1">
                <span className="perfil-header1">Cardiology</span>
                <span className="perfil-caption">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt.
                </span>
              </div>
              <div className="read-more">
                <span className="perfil-text">Read more</span>
                <img
                  alt="image"
                  src="/playground_assets/arrow-2.svg"
                  className="perfil-image"
                />
              </div>
            </div>
            <div className="perfil-section1">
              <div className="perfil-heading2">
                <span className="perfil-header2">Orthopedics</span>
                <span className="perfil-caption1">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt.
                </span>
              </div>
              <div className="read-more">
                <span className="perfil-text01">Read more</span>
                <img
                  alt="image"
                  src="/playground_assets/arrow-2.svg"
                  className="perfil-image01"
                />
              </div>
            </div>
            <div className="perfil-section2">
              <div className="perfil-heading3">
                <span className="perfil-header3">Ophtalmology</span>
                <span className="perfil-caption2">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt.
                </span>
              </div>
              <div className="read-more">
                <span className="perfil-text02">Read more</span>
                <img
                  alt="image"
                  src="/playground_assets/arrow-2.svg"
                  className="perfil-image02"
                />
              </div>
            </div>
            <div className="perfil-section3">
              <div className="perfil-heading4">
                <span className="perfil-header4">Pediatrics</span>
                <span className="perfil-caption3">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt.
                </span>
              </div>
              <div className="read-more">
                <span className="perfil-text03">Read more</span>
                <img
                  alt="image"
                  src="/playground_assets/arrow-2.svg"
                  className="perfil-image03"
                />
              </div>
            </div>
            <div className="perfil-section4">
              <div className="perfil-heading5">
                <span className="perfil-header5">Nutrition</span>
                <span className="perfil-caption4">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt.
                </span>
              </div>
              <div className="read-more">
                <span className="perfil-text04">Read more</span>
                <img
                  alt="image"
                  src="/playground_assets/arrow-2.svg"
                  className="perfil-image04"
                />
              </div>
            </div>
            <div className="perfil-section5">
              <div className="perfil-heading6">
                <span className="perfil-header6">General</span>
                <span className="perfil-caption5">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt.
                </span>
              </div>
              <div className="read-more">
                <span className="perfil-text05">Read more</span>
                <img
                  alt="image"
                  src="/playground_assets/arrow-2.svg"
                  className="perfil-image05"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      <section className="perfil-hero">
        <div className="perfil-container1"></div>
        <div data-thq="thq-dropdown" className="perfil-thq-dropdown list-item">
          <ul data-thq="thq-dropdown-list" className="perfil-dropdown-list">
            <li data-thq="thq-dropdown" className="perfil-dropdown list-item">
              <div
                data-thq="thq-dropdown-toggle"
                className="perfil-dropdown-toggle"
              >
                <span className="perfil-text06">Sub-menu Item</span>
              </div>
            </li>
            <li data-thq="thq-dropdown" className="perfil-dropdown1 list-item">
              <div
                data-thq="thq-dropdown-toggle"
                className="perfil-dropdown-toggle1"
              >
                <span className="perfil-text07">Sub-menu Item</span>
              </div>
            </li>
            <li data-thq="thq-dropdown" className="perfil-dropdown2 list-item">
              <div
                data-thq="thq-dropdown-toggle"
                className="perfil-dropdown-toggle2"
              >
                <span className="perfil-text08">Sub-menu Item</span>
              </div>
            </li>
          </ul>
        </div>
        <header data-thq="thq-navbar" className="perfil-navbar">
          <div className="perfil-left">
            <nav className="perfil-links"></nav>
          </div>
          <div data-thq="thq-navbar-btn-group" className="perfil-right"></div>
          <div data-thq="thq-burger-menu" className="perfil-burger-menu">
            <svg viewBox="0 0 1024 1024" className="perfil-icon01">
              <path d="M128 554.667h768c23.552 0 42.667-19.115 42.667-42.667s-19.115-42.667-42.667-42.667h-768c-23.552 0-42.667 19.115-42.667 42.667s19.115 42.667 42.667 42.667zM128 298.667h768c23.552 0 42.667-19.115 42.667-42.667s-19.115-42.667-42.667-42.667h-768c-23.552 0-42.667 19.115-42.667 42.667s19.115 42.667 42.667 42.667zM128 810.667h768c23.552 0 42.667-19.115 42.667-42.667s-19.115-42.667-42.667-42.667h-768c-23.552 0-42.667 19.115-42.667 42.667s19.115 42.667 42.667 42.667z"></path>
            </svg>
          </div>
          <div data-thq="thq-mobile-menu" className="perfil-mobile-menu">
            <div
              data-thq="thq-mobile-menu-nav"
              data-role="Nav"
              className="perfil-nav"
            >
              <div className="perfil-container2">
                <img
                  alt="image"
                  src="/playground_assets/logo-1500h.png"
                  className="perfil-image06"
                />
                <div data-thq="thq-close-menu" className="perfil-menu-close">
                  <svg viewBox="0 0 1024 1024" className="perfil-icon03">
                    <path d="M810 274l-238 238 238 238-60 60-238-238-238 238-60-60 238-238-238-238 60-60 238 238 238-238z"></path>
                  </svg>
                </div>
              </div>
              <nav
                data-thq="thq-mobile-menu-nav-links"
                data-role="Nav"
                className="perfil-nav1"
              >
                <span className="perfil-text09">Features</span>
                <span className="perfil-text10">How it works</span>
                <span className="perfil-text11">Prices</span>
                <span className="perfil-text12">Contact</span>
                <button className="perfil-book button button-main">
                  <img
                    alt="image"
                    src="/playground_assets/calendar.svg"
                    className="perfil-image07"
                  />
                  <span className="perfil-text13">Book an appointment</span>
                </button>
              </nav>
            </div>
          </div>
          <header data-role="Header" className="perfil-header7">
            <img
              alt="logo"
              src="/playground_assets/logo_transparent-300h.png"
              className="perfil-image08"
            />
            <div className="perfil-nav2">
              <NavigationLinks
                rootClassName="rootClassName10"
                text="Perfil"
                text2="Pagamentos"
                text1="Planners"
                text3="Turmas"
                text4="Alunos"
              ></NavigationLinks>
            </div>
            <div className="perfil-btn-group">
              <button className="perfil-login button">
                <span>
                  <span>Sair</span>
                  <br></br>
                </span>
              </button>
            </div>
            <div data-role="BurgerMenu" className="perfil-burger-menu1">
              <svg viewBox="0 0 1024 1024" className="perfil-icon05">
                <path d="M128 554.667h768c23.552 0 42.667-19.115 42.667-42.667s-19.115-42.667-42.667-42.667h-768c-23.552 0-42.667 19.115-42.667 42.667s19.115 42.667 42.667 42.667zM128 298.667h768c23.552 0 42.667-19.115 42.667-42.667s-19.115-42.667-42.667-42.667h-768c-23.552 0-42.667 19.115-42.667 42.667s19.115 42.667 42.667 42.667zM128 810.667h768c23.552 0 42.667-19.115 42.667-42.667s-19.115-42.667-42.667-42.667h-768c-23.552 0-42.667 19.115-42.667 42.667s19.115 42.667 42.667 42.667z"></path>
              </svg>
            </div>
            <div data-role="MobileMenu" className="perfil-mobile-menu1">
              <div className="perfil-nav3">
                <div className="perfil-container3">
                  <img
                    alt="image"
                    src="https://presentation-website-assets.teleporthq.io/logos/logo.png"
                    className="perfil-image09"
                  />
                  <div
                    data-role="CloseMobileMenu"
                    className="perfil-menu-close1"
                  >
                    <svg viewBox="0 0 1024 1024" className="perfil-icon07">
                      <path d="M810 274l-238 238 238 238-60 60-238-238-238 238-60-60 238-238-238-238 60-60 238 238 238-238z"></path>
                    </svg>
                  </div>
                </div>
                <NavigationLinks rootClassName="rootClassName11"></NavigationLinks>
              </div>
              <div>
                <svg
                  viewBox="0 0 950.8571428571428 1024"
                  className="perfil-icon09"
                >
                  <path d="M925.714 233.143c-25.143 36.571-56.571 69.143-92.571 95.429 0.571 8 0.571 16 0.571 24 0 244-185.714 525.143-525.143 525.143-104.571 0-201.714-30.286-283.429-82.857 14.857 1.714 29.143 2.286 44.571 2.286 86.286 0 165.714-29.143 229.143-78.857-81.143-1.714-149.143-54.857-172.571-128 11.429 1.714 22.857 2.857 34.857 2.857 16.571 0 33.143-2.286 48.571-6.286-84.571-17.143-148-91.429-148-181.143v-2.286c24.571 13.714 53.143 22.286 83.429 23.429-49.714-33.143-82.286-89.714-82.286-153.714 0-34.286 9.143-65.714 25.143-93.143 90.857 112 227.429 185.143 380.571 193.143-2.857-13.714-4.571-28-4.571-42.286 0-101.714 82.286-184.571 184.571-184.571 53.143 0 101.143 22.286 134.857 58.286 41.714-8 81.714-23.429 117.143-44.571-13.714 42.857-42.857 78.857-81.143 101.714 37.143-4 73.143-14.286 106.286-28.571z"></path>
                </svg>
                <svg
                  viewBox="0 0 877.7142857142857 1024"
                  className="perfil-icon11"
                >
                  <path d="M585.143 512c0-80.571-65.714-146.286-146.286-146.286s-146.286 65.714-146.286 146.286 65.714 146.286 146.286 146.286 146.286-65.714 146.286-146.286zM664 512c0 124.571-100.571 225.143-225.143 225.143s-225.143-100.571-225.143-225.143 100.571-225.143 225.143-225.143 225.143 100.571 225.143 225.143zM725.714 277.714c0 29.143-23.429 52.571-52.571 52.571s-52.571-23.429-52.571-52.571 23.429-52.571 52.571-52.571 52.571 23.429 52.571 52.571zM438.857 152c-64 0-201.143-5.143-258.857 17.714-20 8-34.857 17.714-50.286 33.143s-25.143 30.286-33.143 50.286c-22.857 57.714-17.714 194.857-17.714 258.857s-5.143 201.143 17.714 258.857c8 20 17.714 34.857 33.143 50.286s30.286 25.143 50.286 33.143c57.714 22.857 194.857 17.714 258.857 17.714s201.143 5.143 258.857-17.714c20-8 34.857-17.714 50.286-33.143s25.143-30.286 33.143-50.286c22.857-57.714 17.714-194.857 17.714-258.857s5.143-201.143-17.714-258.857c-8-20-17.714-34.857-33.143-50.286s-30.286-25.143-50.286-33.143c-57.714-22.857-194.857-17.714-258.857-17.714zM877.714 512c0 60.571 0.571 120.571-2.857 181.143-3.429 70.286-19.429 132.571-70.857 184s-113.714 67.429-184 70.857c-60.571 3.429-120.571 2.857-181.143 2.857s-120.571 0.571-181.143-2.857c-70.286-3.429-132.571-19.429-184-70.857s-67.429-113.714-70.857-184c-3.429-60.571-2.857-120.571-2.857-181.143s-0.571-120.571 2.857-181.143c3.429-70.286 19.429-132.571 70.857-184s113.714-67.429 184-70.857c60.571-3.429 120.571-2.857 181.143-2.857s120.571-0.571 181.143 2.857c70.286 3.429 132.571 19.429 184 70.857s67.429 113.714 70.857 184c3.429 60.571 2.857 120.571 2.857 181.143z"></path>
                </svg>
                <svg
                  viewBox="0 0 602.2582857142856 1024"
                  className="perfil-icon13"
                >
                  <path d="M548 6.857v150.857h-89.714c-70.286 0-83.429 33.714-83.429 82.286v108h167.429l-22.286 169.143h-145.143v433.714h-174.857v-433.714h-145.714v-169.143h145.714v-124.571c0-144.571 88.571-223.429 217.714-223.429 61.714 0 114.857 4.571 130.286 6.857z"></path>
                </svg>
              </div>
            </div>
          </header>
        </header>
      </section>
      <div className="perfil-download"></div>
      <div className="perfil-footer">
        <div className="perfil-left1">
          <div className="perfil-brand">
            <img
              alt="image"
              src="/playground_assets/logo_transparent-300h.png"
              className="perfil-image10"
            />
          </div>
          <div className="perfil-legal"></div>
        </div>
        <p className="perfil-text17">
          Aplicação desenvolvida pelos alunos do eixo computação da Universidade
          Virtual de São Paulo.
        </p>
        <div className="perfil-legal1">
          <div className="perfil-row">
            <span className="legal-link">Privacy Policy</span>
            <span className="legal-link">Terms of Use</span>
          </div>
          <span className="perfil-copyright2">
            © 2022 finbest. All Rights Reserved.
          </span>
        </div>
      </div>
      <div>
        <DangerousHTML
          html={`<script>
const modalOpen = document.querySelectorAll('[data-open]');
const modalClose = document.querySelectorAll('[data-close]');

modalOpen.forEach(button => {
    button.addEventListener('click', event => {
        const modal = document.querySelector(\`[data-modal="\${event.target.dataset.open}"]\`);
        modal.style.display = "flex";
    });
});

modalClose.forEach(button => {
    button.addEventListener('click', event => {
        const modal = document.querySelector(\`[data-modal="\${event.target.dataset.close}"]\`);
        modal.style.display = "none";
    });
});
</script>
`}
        ></DangerousHTML>
      </div>
      <div>
        <DangerousHTML
          html={`<script>
const dataLetters = document.querySelectorAll("[data-letter]");
let activeLetters = [];
const maxResults = 6;

dataLetters.forEach(letter => {
  letter.addEventListener("click", function() {
    if (this.classList.contains("letter-active")) {
      this.classList.remove("letter-active");
      activeLetters = activeLetters.filter(a => a !== this.dataset.letter);
    } else {
      this.classList.add("letter-active");
      activeLetters.push(this.dataset.letter);
    }
    if (activeLetters.length == 0) {
      document.querySelector("[data-teleport='results']").style.display = "none";
      return;
    }
    showResults();
  });
});

const showResults = () => {
  fetch("https://raw.githubusercontent.com/Shivanshu-Gupta/web-scrapers/master/medical_ner/medicinenet-diseases.json")
    .then(response => response.json())
    .then(data => {
      const filteredData = data.filter(item => {
        const firstLetter = item.disease.charAt(0).toLowerCase();
        if (activeLetters.includes(firstLetter)) {
          return true;
        }
        return false;
      });

      document.querySelector("[data-teleport='results']").style.display = "flex";
      const resultsContainer = document.querySelector("[data-results='letters']");
      resultsContainer.innerHTML = "";

      let counter = 0;
      const diseaseGroups = {};
      const totalActiveLetters = activeLetters.length;

      filteredData.forEach(disease => {
        const firstLetter = disease.disease[0].toLowerCase();
        if (diseaseGroups[firstLetter]) {
          diseaseGroups[firstLetter].push(disease);
        } else {
          diseaseGroups[firstLetter] = [disease];
        }
      });

      Object.keys(diseaseGroups).sort().forEach((firstLetter, index) => {
        const diseasesForThisLetter = diseaseGroups[firstLetter];
        const diseasesToShow = diseasesForThisLetter.slice(0, Math.ceil(maxResults / totalActiveLetters));

        diseasesToShow.forEach(disease => {
          const resultContainer = document.createElement("div");
          resultContainer.classList.add("search-result");
          resultContainer.classList.add("invisible");
          resultContainer.style.animationDelay = \`\${counter * 0.25}s\`;

          const resultText = document.createElement("span");
          resultText.classList.add("result-text");
          resultText.textContent = disease.disease;

          resultContainer.appendChild(resultText);
          resultsContainer.appendChild(resultContainer);
          counter++;

          if (counter === maxResults) {
            const moreContainer = document.createElement("div");
            moreContainer.classList.add("search-result");
            moreContainer.classList.add("more-results");

            const moreText = document.createElement("span");
            moreText.classList.add("result-text");
            moreText.textContent = "More";

            moreContainer.appendChild(moreText);
            resultsContainer.appendChild(moreContainer);
            addedMoreContainer = true;
            return;
          }
        });
      });
    });
};
</script>
`}
        ></DangerousHTML>
      </div>
      <div>
        <DangerousHTML
          html={`<script>
function scroll(direction) {
  const doctorsDiv = document.querySelector('[data-teleport="doctors"]');
  const scrollAmount = 300;
  if (direction === 'previous') {
    doctorsDiv.scrollBy({
      left: -scrollAmount,
      behavior: 'smooth'
    });
  } else if (direction === 'next') {
    doctorsDiv.scrollBy({
      left: scrollAmount,
      behavior: 'smooth'
    });
  }
}

const buttons = document.querySelectorAll('[data-doctors]');
buttons.forEach(button => {
  button.addEventListener('click', () => {
    const direction = button.dataset.doctors;
    scroll(direction);
  });
});
</script>`}
        ></DangerousHTML>
      </div>
      <li className="list-item">
        <span>Text</span>
      </li>
    </div>
  )
}

export default Perfil
