import React from 'react'

import DangerousHTML from 'dangerous-html/react'
import { Helmet } from 'react-helmet'

import './sign-up.css'

const SignUp = (props) => {
  return (
    <div className="sign-up-container">
      <Helmet>
        <title>Sign-up - Medica template</title>
        <meta property="og:title" content="Sign-up - Medica template" />
      </Helmet>
      <div data-modal="practices" className="sign-up-modal">
        <div className="sign-up-practices">
          <div className="sign-up-heading">
            <span className="sign-up-header">Our practices</span>
            <svg
              viewBox="0 0 1024 1024"
              data-close="practices"
              className="sign-up-close"
            >
              <path d="M225.835 286.165l225.835 225.835-225.835 225.835c-16.683 16.683-16.683 43.691 0 60.331s43.691 16.683 60.331 0l225.835-225.835 225.835 225.835c16.683 16.683 43.691 16.683 60.331 0s16.683-43.691 0-60.331l-225.835-225.835 225.835-225.835c16.683-16.683 16.683-43.691 0-60.331s-43.691-16.683-60.331 0l-225.835 225.835-225.835-225.835c-16.683-16.683-43.691-16.683-60.331 0s-16.683 43.691 0 60.331z"></path>
            </svg>
          </div>
          <div className="sign-up-grid">
            <div className="sign-up-section">
              <div className="sign-up-heading1">
                <span className="sign-up-header1">Cardiology</span>
                <span className="sign-up-caption">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt.
                </span>
              </div>
              <div className="read-more">
                <span className="sign-up-text">Read more</span>
                <img
                  alt="image"
                  src="/playground_assets/arrow-2.svg"
                  className="sign-up-image"
                />
              </div>
            </div>
            <div className="sign-up-section1">
              <div className="sign-up-heading2">
                <span className="sign-up-header2">Orthopedics</span>
                <span className="sign-up-caption1">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt.
                </span>
              </div>
              <div className="read-more">
                <span className="sign-up-text01">Read more</span>
                <img
                  alt="image"
                  src="/playground_assets/arrow-2.svg"
                  className="sign-up-image1"
                />
              </div>
            </div>
            <div className="sign-up-section2">
              <div className="sign-up-heading3">
                <span className="sign-up-header3">Ophtalmology</span>
                <span className="sign-up-caption2">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt.
                </span>
              </div>
              <div className="read-more">
                <span className="sign-up-text02">Read more</span>
                <img
                  alt="image"
                  src="/playground_assets/arrow-2.svg"
                  className="sign-up-image2"
                />
              </div>
            </div>
            <div className="sign-up-section3">
              <div className="sign-up-heading4">
                <span className="sign-up-header4">Pediatrics</span>
                <span className="sign-up-caption3">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt.
                </span>
              </div>
              <div className="read-more">
                <span className="sign-up-text03">Read more</span>
                <img
                  alt="image"
                  src="/playground_assets/arrow-2.svg"
                  className="sign-up-image3"
                />
              </div>
            </div>
            <div className="sign-up-section4">
              <div className="sign-up-heading5">
                <span className="sign-up-header5">Nutrition</span>
                <span className="sign-up-caption4">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt.
                </span>
              </div>
              <div className="read-more">
                <span className="sign-up-text04">Read more</span>
                <img
                  alt="image"
                  src="/playground_assets/arrow-2.svg"
                  className="sign-up-image4"
                />
              </div>
            </div>
            <div className="sign-up-section5">
              <div className="sign-up-heading6">
                <span className="sign-up-header6">General</span>
                <span className="sign-up-caption5">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt.
                </span>
              </div>
              <div className="read-more">
                <span className="sign-up-text05">Read more</span>
                <img
                  alt="image"
                  src="/playground_assets/arrow-2.svg"
                  className="sign-up-image5"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      <section className="sign-up-hero">
        <header data-thq="thq-navbar" className="sign-up-navbar">
          <div className="sign-up-left">
            <img
              alt="image"
              src="/playground_assets/logo_transparent-300h.png"
              className="sign-up-logo"
            />
            <nav className="sign-up-links">
              <a href="#features" className="sign-up-link">
                Sobre
              </a>
              <span className="sign-up-link1">Ferramentas</span>
              <span className="sign-up-link2">FAQ</span>
            </nav>
          </div>
          <div data-thq="thq-navbar-btn-group" className="sign-up-right"></div>
          <div className="sign-up-container1"></div>
          <div data-thq="thq-burger-menu" className="sign-up-burger-menu">
            <svg viewBox="0 0 1024 1024" className="sign-up-icon1">
              <path d="M128 554.667h768c23.552 0 42.667-19.115 42.667-42.667s-19.115-42.667-42.667-42.667h-768c-23.552 0-42.667 19.115-42.667 42.667s19.115 42.667 42.667 42.667zM128 298.667h768c23.552 0 42.667-19.115 42.667-42.667s-19.115-42.667-42.667-42.667h-768c-23.552 0-42.667 19.115-42.667 42.667s19.115 42.667 42.667 42.667zM128 810.667h768c23.552 0 42.667-19.115 42.667-42.667s-19.115-42.667-42.667-42.667h-768c-23.552 0-42.667 19.115-42.667 42.667s19.115 42.667 42.667 42.667z"></path>
            </svg>
          </div>
          <div data-thq="thq-mobile-menu" className="sign-up-mobile-menu">
            <div
              data-thq="thq-mobile-menu-nav"
              data-role="Nav"
              className="sign-up-nav"
            >
              <div className="sign-up-container2">
                <img
                  alt="image"
                  src="/playground_assets/logo-1500h.png"
                  className="sign-up-image6"
                />
                <div data-thq="thq-close-menu" className="sign-up-menu-close">
                  <svg viewBox="0 0 1024 1024" className="sign-up-icon3">
                    <path d="M810 274l-238 238 238 238-60 60-238-238-238 238-60-60 238-238-238-238 60-60 238 238 238-238z"></path>
                  </svg>
                </div>
              </div>
              <nav
                data-thq="thq-mobile-menu-nav-links"
                data-role="Nav"
                className="sign-up-nav1"
              >
                <span className="sign-up-text06">Features</span>
                <span className="sign-up-text07">How it works</span>
                <span className="sign-up-text08">Prices</span>
                <span className="sign-up-text09">Contact</span>
                <button className="sign-up-book button button-main">
                  <img
                    alt="image"
                    src="/playground_assets/calendar.svg"
                    className="sign-up-image7"
                  />
                  <span className="sign-up-text10">Book an appointment</span>
                </button>
              </nav>
            </div>
          </div>
        </header>
        <div className="sign-up-main">
          <div className="sign-up-content">
            <div className="sign-up-heading7">
              <h1 className="sign-up-header7">
                A ferramenta de organização que todo professor precisa.
              </h1>
            </div>
          </div>
          <div className="sign-up-container3">
            <form className="sign-up-form">
              <div className="sign-up-container4">
                <h1>Cadastro</h1>
              </div>
              <div className="sign-up-container5">
                <input
                  type="text"
                  placeholder="Nome completo"
                  className="sign-up-textinput input"
                />
              </div>
              <div className="sign-up-container6">
                <input
                  type="text"
                  placeholder="email"
                  className="sign-up-textinput1 input"
                />
              </div>
              <div className="sign-up-container7">
                <input
                  type="text"
                  placeholder="Senha"
                  className="sign-up-textinput2 input"
                />
              </div>
              <button name="signup" type="submit" className="button">
                <span className="button button-main">
                  <span>Cadastrar</span>
                  <br></br>
                </span>
              </button>
            </form>
          </div>
        </div>
        <div id="features" className="sign-up-features">
          <div className="sign-up-content1"></div>
        </div>
        <div className="sign-up-background"></div>
      </section>
      <div className="sign-up-download"></div>
      <div className="sign-up-footer">
        <div className="sign-up-left1">
          <div className="sign-up-brand">
            <img
              alt="image"
              src="/playground_assets/logo_transparent-300h.png"
              className="sign-up-image8"
            />
          </div>
          <div className="sign-up-legal"></div>
        </div>
        <p className="sign-up-text15">
          Aplicação desenvolvida pelos alunos do eixo computação da Universidade
          Virtual de São Paulo.
        </p>
        <div className="sign-up-legal1">
          <div className="sign-up-row">
            <span className="legal-link">Privacy Policy</span>
            <span className="legal-link">Terms of Use</span>
          </div>
          <span className="sign-up-copyright2">
            © 2022 finbest. All Rights Reserved.
          </span>
        </div>
      </div>
      <div>
        <DangerousHTML
          html={`<script>
const modalOpen = document.querySelectorAll('[data-open]');
const modalClose = document.querySelectorAll('[data-close]');

modalOpen.forEach(button => {
    button.addEventListener('click', event => {
        const modal = document.querySelector(\`[data-modal="\${event.target.dataset.open}"]\`);
        modal.style.display = "flex";
    });
});

modalClose.forEach(button => {
    button.addEventListener('click', event => {
        const modal = document.querySelector(\`[data-modal="\${event.target.dataset.close}"]\`);
        modal.style.display = "none";
    });
});
</script>
`}
        ></DangerousHTML>
      </div>
      <div>
        <DangerousHTML
          html={`<script>
const dataLetters = document.querySelectorAll("[data-letter]");
let activeLetters = [];
const maxResults = 6;

dataLetters.forEach(letter => {
  letter.addEventListener("click", function() {
    if (this.classList.contains("letter-active")) {
      this.classList.remove("letter-active");
      activeLetters = activeLetters.filter(a => a !== this.dataset.letter);
    } else {
      this.classList.add("letter-active");
      activeLetters.push(this.dataset.letter);
    }
    if (activeLetters.length == 0) {
      document.querySelector("[data-teleport='results']").style.display = "none";
      return;
    }
    showResults();
  });
});

const showResults = () => {
  fetch("https://raw.githubusercontent.com/Shivanshu-Gupta/web-scrapers/master/medical_ner/medicinenet-diseases.json")
    .then(response => response.json())
    .then(data => {
      const filteredData = data.filter(item => {
        const firstLetter = item.disease.charAt(0).toLowerCase();
        if (activeLetters.includes(firstLetter)) {
          return true;
        }
        return false;
      });

      document.querySelector("[data-teleport='results']").style.display = "flex";
      const resultsContainer = document.querySelector("[data-results='letters']");
      resultsContainer.innerHTML = "";

      let counter = 0;
      const diseaseGroups = {};
      const totalActiveLetters = activeLetters.length;

      filteredData.forEach(disease => {
        const firstLetter = disease.disease[0].toLowerCase();
        if (diseaseGroups[firstLetter]) {
          diseaseGroups[firstLetter].push(disease);
        } else {
          diseaseGroups[firstLetter] = [disease];
        }
      });

      Object.keys(diseaseGroups).sort().forEach((firstLetter, index) => {
        const diseasesForThisLetter = diseaseGroups[firstLetter];
        const diseasesToShow = diseasesForThisLetter.slice(0, Math.ceil(maxResults / totalActiveLetters));

        diseasesToShow.forEach(disease => {
          const resultContainer = document.createElement("div");
          resultContainer.classList.add("search-result");
          resultContainer.classList.add("invisible");
          resultContainer.style.animationDelay = \`\${counter * 0.25}s\`;

          const resultText = document.createElement("span");
          resultText.classList.add("result-text");
          resultText.textContent = disease.disease;

          resultContainer.appendChild(resultText);
          resultsContainer.appendChild(resultContainer);
          counter++;

          if (counter === maxResults) {
            const moreContainer = document.createElement("div");
            moreContainer.classList.add("search-result");
            moreContainer.classList.add("more-results");

            const moreText = document.createElement("span");
            moreText.classList.add("result-text");
            moreText.textContent = "More";

            moreContainer.appendChild(moreText);
            resultsContainer.appendChild(moreContainer);
            addedMoreContainer = true;
            return;
          }
        });
      });
    });
};
</script>
`}
        ></DangerousHTML>
      </div>
      <div>
        <DangerousHTML
          html={`<script>
function scroll(direction) {
  const doctorsDiv = document.querySelector('[data-teleport="doctors"]');
  const scrollAmount = 300;
  if (direction === 'previous') {
    doctorsDiv.scrollBy({
      left: -scrollAmount,
      behavior: 'smooth'
    });
  } else if (direction === 'next') {
    doctorsDiv.scrollBy({
      left: scrollAmount,
      behavior: 'smooth'
    });
  }
}

const buttons = document.querySelectorAll('[data-doctors]');
buttons.forEach(button => {
  button.addEventListener('click', () => {
    const direction = button.dataset.doctors;
    scroll(direction);
  });
});
</script>`}
        ></DangerousHTML>
      </div>
    </div>
  )
}

export default SignUp
