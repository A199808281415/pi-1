import React from 'react'

import PropTypes from 'prop-types'

import './list.css'

const List = (props) => {
  return (
    <div className={`list-list ${props.rootClassName} `}>
      <div className="list-controls"></div>
    </div>
  )
}

List.defaultProps = {
  rootClassName: '',
}

List.propTypes = {
  rootClassName: PropTypes.string,
}

export default List
