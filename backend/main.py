from fastapi import FastAPI
from sqlmodel import SQLModel
from fastapi.middleware.cors import CORSMiddleware

from .models import Aluno, AlunoTurmaLink, Disciplina, Pagamento, Professor, Turma, engine
from .routes import disciplina, professor, aluno

app = FastAPI()
app.include_router(disciplina.router)
app.include_router(professor.router)
app.include_router(aluno.router)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"]
)

@app.on_event("startup")
async def startup():
    SQLModel.metadata.create_all(engine)


@app.get("/")
async def root():
    return {"message": "Hello, World!"}
